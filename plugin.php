<?php
/*
  Plugin Name: qTranslate Extension Pack
  Plugin URI: http://fractalia.pe/
  Description: qTranslate extension with additional features and the possibility of converting plugins in multilanguage
  Author: Fractalia - Applications lab
  Autor URI: http://fractalia.pe/
  Version: 0.1.1
  Tags: fractalia, wordpress, qtranslate, qtranslate extension, plugin, multilanguage
 */

add_action('plugins_loaded', 'qtransep_init', 3);

function qtransep_init()
{
    global $q_config;

    if (!isset($q_config)) {
        return;
    }

    if (!isset($q_config['language'])) {
        $q_config['language'] = 'en';
    }

    add_action('admin_footer', 'qtransep_enable_javascript', 0);
}

function qtransep_enable_javascript()
{
    global $q_config;

    foreach ($q_config['enabled_languages'] as $iso_code) {
        $enabled_languages[] = "'{$iso_code}'";
        $flags[] = "'" . content_url('plugins/qtranslate/flags/') . "{$q_config['flag'][$iso_code]}'";
    }
    ?>
    <link rel="stylesheet" href="<?php echo plugins_url('/qtranslate-extension-pack.css', __FILE__) ?>" />
    <script src="<?php echo plugins_url('/qtranslate-extension-pack.js', __FILE__) ?>"></script>
    <script>
        var qtransepLanguages = [<?php echo implode(',', $enabled_languages) ?>];
        var qtransepFlags = [<?php echo implode(',', $flags) ?>];

        jQuery(document).ready(function ($) {
            if (qtransepLanguages.length === 1) {
                return;
            }

            $('body').on('click', 'span.multilanguage-flags img', function (evt) {
                var $img = $(evt.currentTarget);
                $img.parent().find('.current').removeClass('current');
                $img.addClass('current');
                $img.parent().prev('span').find('.current').removeClass('current');
                $img.parent().prev('span').find('[lang="' + $img.attr('alt') + '"]').addClass('current').focus();
            });

            qtransepGenerateMultilanguageElements(qtransepLanguages, qtransepFlags);
        });
    </script>
    <?php
}

function qtransep_enable_support_for_taxonomies()
{
    $args = array(
        'public' => true,
        '_builtin' => false
    );
    $output = 'object'; // or objects
    $operator = 'and'; // 'and' or 'or'

    $taxonomies = get_taxonomies($args, $output, $operator);

    if ($taxonomies) {
        foreach ($taxonomies as $taxonomy) {
            add_action($taxonomy->name . '_add_form', 'qtrans_modifyTermFormFor');
            add_action($taxonomy->name . '_edit_form', 'qtrans_modifyTermFormFor');
        }
    }
}

add_action('admin_init', 'qtransep_enable_support_for_taxonomies');

function qtransep_remove_empty_content($content)
{
    $content = preg_replace('/<!--:[a-z]{2}-->(&nbsp;|\s|\n|\r)*<!--:-->/', '', $content);
    return $content;
}

add_filter('content_save_pre', 'qtransep_remove_empty_content');

function qtransep_the_editor_fix($editor)
{
    global $q_config;

    if (!(isset($q_config['js']) && isset($q_config['js']['qtrans_switch']))) {
        return $editor;
    }

    $q_config['js']['qtrans_switch'] = "
		switchEditors.go = function(id, lang) {
			id = id || 'content';
			lang = lang || 'toggle';
			
			if ( 'toggle' == lang ) {
				if ( ed && !ed.isHidden() ){
					lang = 'html';
				} else {
					lang = 'tmce';
                                        }
			} else if( 'tinymce' == lang ) {
				lang = 'tmce';
                        }

                        var inst = tinyMCE.get('qtrans_textarea_' + id);
			var vta = document.getElementById('qtrans_textarea_' + id);
			var ta = document.getElementById(id);
			var dom = tinymce.DOM;
			var wrap_id = 'wp-'+id+'-wrap';
                        console.log(id);console.log(lang);
                        if(null === vta){
                            var inst = tinyMCE.get(id);                            
                            var vta = document.getElementById(id);
                        }
			
			// update merged content
			if(inst && ! inst.isHidden()) {
				tinyMCE.triggerSave();console.log('b');
			} else {
				qtrans_save(vta.value);console.log('c');
			}
			
			// check if language is already active
			if(lang!='tmce' && lang!='html' && document.getElementById('qtrans_select_'+lang).className=='wp-switch-editor switch-tmce switch-html') {
                        console.log('d');
				return;
			}
			
			if(lang!='tmce' && lang!='html') {console.log('e');
				document.getElementById('qtrans_select_'+qtrans_get_active_language()).className='wp-switch-editor';
				document.getElementById('qtrans_select_'+lang).className='wp-switch-editor switch-tmce switch-html';
			}
			
			if(lang=='html') {console.log('f');
				if ( inst && inst.isHidden() )
					return false;
				if ( inst ) {
					vta.style.height = inst.getContentAreaContainer().offsetHeight + 20 + 'px';
					inst.hide();
				}
				
				dom.removeClass(wrap_id, 'tmce-active');
				dom.addClass(wrap_id, 'html-active');
				setUserSetting( 'editor', 'html' );
			} else if(lang=='tmce') {console.log('g');
				if(inst && ! inst.isHidden()){
					return false;
                                        }
				if ( typeof(QTags) != 'undefined' ){
					QTags.closeAllTags(id);
                                        }
				if ( tinyMCEPreInit.mceInit['qtrans_textarea_'+id] && tinyMCEPreInit.mceInit['qtrans_textarea_'+id].wpautop ){
					vta.value = this.wpautop(qtrans_use(qtrans_get_active_language(),ta.value));
                                        }
				if (inst) {
					inst.show();
				} else {
					qtrans_hook_on_tinyMCE('qtrans_textarea_'+id);
				}
				
				dom.removeClass(wrap_id, 'html-active');
				dom.addClass(wrap_id, 'tmce-active');
				setUserSetting('editor', 'tinymce');
			} else {
                        console.log('f');
				// switch content
                                console.log('qtrans_textarea_'+id);
                                console.log(lang);
                                console.log(ta.value);
				qtrans_assign('qtrans_textarea_'+id,qtrans_use(lang, ta.value));
			}
		}
		";

    return $editor;
}

add_action('the_editor', 'qtransep_the_editor_fix', 9);
