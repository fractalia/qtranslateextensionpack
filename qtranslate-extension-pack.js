function uniqid(prefix, more_entropy) {
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +    revised by: Kankrelune (http://www.webfaktory.info/)
    // %        note 1: Uses an internal counter (in php_js global) to avoid collision
    // *     example 1: uniqid();
    // *     returns 1: 'a30285b160c14'
    // *     example 2: uniqid('foo');
    // *     returns 2: 'fooa30285b1cd361'
    // *     example 3: uniqid('bar', true);
    // *     returns 3: 'bara20285b23dfd1.31879087'

    if (typeof prefix === 'undefined') {
        prefix = "";
    }

    var retId;

    var formatSeed = function(seed, reqWidth) {
        seed = parseInt(seed, 10).toString(16); // to hex str
        if (reqWidth < seed.length) { // so long we split
            return seed.slice(seed.length - reqWidth);
        }
        if (reqWidth > seed.length) { // so short we pad
            return Array(1 + (reqWidth - seed.length)).join('0') + seed;
        }
        return seed;
    };

    // BEGIN REDUNDANT
    if (!this.php_js) {
        this.php_js = {};
    }
    // END REDUNDANT
    if (!this.php_js.uniqidSeed) { // init seed with big random int
        this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
    }
    this.php_js.uniqidSeed++;

    retId = prefix; // start with prefix, add current milliseconds hex string
    retId += formatSeed(parseInt(new Date().getTime() / 1000, 10), 8);
    retId += formatSeed(this.php_js.uniqidSeed, 5); // add seed hex string

    if (more_entropy) {
        // for more entropy we add a float lower to 10
        retId += (Math.random() * 10).toFixed(8).toString();
    }

    return retId;
}

function qtransepPrepareMultilanguageData(form, languages) {
    jQuery(form).find('[data-multilanguage]').each(function(i, e) {
        var content = '', val;

        switch (e.tagName) {
            case 'INPUT':
            case 'TEXTAREA':
                for (i in languages) {
                    val = jQuery('#qtransep_' + e.id + '_' + languages[i]).val();

                    if (jQuery.trim(val) !== '') {
                        content += '<!--:' + languages[i] + '-->' + val + '<!--:-->';
                    }
                }

                jQuery(e).val(content);

                break;
            case 'DIV':
                for (i in languages) {
                    tinyMCE.getInstanceById('qtransep_' + e.id + '_' + languages[i] + '-textarea').save();

                    val = jQuery('#qtransep_' + e.id + '_' + languages[i] + '-textarea').val();

                    if (jQuery.trim(val) !== '') {
                        content += '<!--:' + languages[i] + '-->' + val + '<!--:-->';
                    }
                }

                jQuery(e).find('textarea').val(content);

                break;
        }

    });
}

function qtransepGetElementValueByLanguage($e, language) {
    var value;

    if ($e.val().match('<!--:([a-z]{2})-->')) {
        value = ('' === $e.val()) ? '' : $e.val().match('<!--:' + language + '-->(.|\n)*?<!--:-->');
        value = (value) ? value[0].replace('<!--:' + language + '-->', '').replace('<!--:-->', '') : '';
    } else {
        value = $e.val();
    }

    return value;
}

function qtransepGenerateMultilanguageElements(languages, flags, context) {
    if (!context) {
        context = 'body';
    }

    jQuery('form', context).each(function(i, form) {
        var f = jQuery(form);

        if (f.find('[data-multilanguage]').length === 0) {
            return;
        }

        f.find('[data-multilanguage]').each(function(j, elem) {
            if (elem.id === '') {
                elem.id = uniqid();
            }

            if (f.find('#qtransep-' + elem.id).length === 1) {
                return;
            }

            var $e = jQuery(elem);

            $e.data('multilanguage', {required: $e.prop('required')});

            var $mElem = jQuery('<div />', {
                'id': 'qtransep-' + elem.id,
                'rel': elem.id,
                'css': {
                    'width': $e.outerWidth()
                },
                'class': 'multilanguage-element',
                'html': '<span class="multilanguage-inputs"></span><span class="multilanguage-flags"></span>'
            });

            $e.hide().after($mElem);

            var $mElemInput, value = '';

            for (i in languages) {
                var $mElemContainer, $mElemInput;

                switch ($e[0].tagName) {
                    case 'INPUT':
                    case 'TEXTAREA':
                        $mElemContainer = $e.clone();
                        $mElemInput = $mElemContainer;
                        break;
                    case 'DIV':
                        $mElemContainer = $e.clone();
                        $mElemInput = $mElemContainer.find('textarea');
                        break;
                }

                value = qtransepGetElementValueByLanguage($mElemInput, languages[i]);

                $mElemContainer
                        .attr('id', 'qtransep_' + elem.id + '_' + languages[i])
                        .attr('lang', languages[i])
                        .removeAttr('data-multilanguage');

                $mElemInput
                        .removeAttr('name')
                        .val(value);

                $mElem.find('span.multilanguage-inputs').append($mElemContainer);
                $mElem.find('span.multilanguage-flags').append('<img alt="' + languages[i] + '" src="' + flags[i] + '" />');
            }

            $e
                    .prop('required', false)
                    .attr('tabindex', '-1');

            $mElem.find('span.multilanguage-inputs > :first, span.multilanguage-flags > :first').addClass('current');
        });

        if (!f.hasClass('multilanguage-form')) {
            f.addClass('multilanguage-form');

            f.on('submit', function(evt) {
                qtransepPrepareMultilanguageData(evt.currentTarget, languages);
            });
        }
    });
}