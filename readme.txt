=== Plugin Name ===
Contributors: Fractalia - Applications Lab
Tags: qtranslate, multilanguage, wordpress, fractalia, qtranslate extension, plugins
Requires at least: 3.0
Tested up to: 3.2.1
Stable tag: trunk

== Description ==

This plugin is based on qtranslate and make other plugins multilanguage. Also add a multilanguage widget for use in sidebars.

Compatible with wordpress 3.0+

== Installation ==

Copy the qtranslate-extended file in wp-content/plugins and activate it.

== Changelog ==

0.1 Initial version